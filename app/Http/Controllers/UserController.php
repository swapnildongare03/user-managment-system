<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function getUsers()
    {
        $users = User::all();

        return view('welcome', ['users' => $users]);
    }

    public function registerUSer(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'email' => 'required | email|unique:users,email',
            'fullname' => 'required | string',
            'joiningDate' => 'required | date',
            'stillWorking' => 'nullable',
            'leavingDate' => 'nullable | date',
            'imageUpload' => 'required | file'

        ]);
        if ($validator->fails()) {
            flash($validator->errors())->warning();
            return redirect()->back();
        }
        $user = new User();

        $user->name = $req->fullname;
        $user->email = $req->email;
        $user->date_of_joining = $req->joiningDate;
        if ($req->leavingDate) {
            if (strtotime($req->leavingDate) < strtotime($req->joiningDate)) {
                flash("Please select valid dates !")->warning();
                return redirect('/');
            }
            $user->date_of_leaving = $req->leavingDate;
        }

        $path = $req->imageUpload->store('user/images', 'public');
        $user->profile_url = config('app.url') . '/storage/' . $path;

        $user->save();
        flash("User Registered")->success();
        return redirect('/');
    }

    public function removeUser(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'user_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $user = User::find($req->user_id);
        $user->delete();

        flash("User removed")->warning();

        return redirect('/');
    }
}
