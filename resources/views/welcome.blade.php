<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>User Managment System</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"
        integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous">
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>

    <div class="container  mt-5" >
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8" >
                <span id="flashMessage">@include('flash::message')</span>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addSUerModal" tabindex="-1" aria-labelledby="addUser" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addUser">Add new record</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group mt-2">
                            <label class="text-secondary" for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group mt-2">
                            <label class="text-secondary" for="fullname">Fullname</label>
                            <input type="text" class="form-control" id="fullname" name="fullname">
                        </div>
                        <div class="form-group mt-2">
                            <label class="text-secondary" for="dateOfJoining">Date of Joining</label>
                            <input type="date" class="form-control" name="joiningDate" id="dateOfJoining">
                        </div>
                        <div class="form-group mt-2">
                            <div class="row">
                                <div class="col-7">
                                    <label class="text-secondary" for="dateOfLeaving">Date of leaving</label>
                                    <input type="date" class="form-control" name="leavingDate" id="dateOfLeaving">
                                </div>
                                <div class="col-4 text-center">
                                    <div class="form-group mt-4">
                                        <input type="checkbox" name="stillWorking" id="stillWorking"
                                            name="stillWorking">
                                        <label class="text-secondary" for="stillWorking">Still Working</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-2">
                            <label class="text-secondary" for="imageUpload">Upload Image</label>
                            <input type="file" class="form-control" name="imageUpload" id="" accept="image/*">
                        </div>
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid card  p-5 mt-5">
        <div class="row">
            <div class="col-10">
                <h3>User Records</h3>
            </div>
            <div class="col-2">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addSUerModal">
                    Add new
                </button>
            </div>
        </div>

        <div class="row text-center p-2 bg-light mt-3 border border-dark" style="font-weight:bold;">
            <div class="col-2">
                Avtar
            </div>
            <div class="col-2">
                Name
            </div>
            <div class="col-3">
                Email
            </div>
            <div class="col-3">
                Experience
            </div>
            <div class="col-2">
                Action
            </div>
        </div>

        @if ($users != null)
            @foreach ($users as $user)
                <div class="row text-center align-items-center font-weight-bold p-3 border border-dark">
                    <div class="col-2">
                        <img src="{{ $user->profile_url }}" alt="" class="img-fluid img-thumbnail rounded-circle"
                            width="70%">
                    </div>
                    <div class="col-2">
                        {{ $user->name }}
                    </div>
                    <div class="col-3">
                        {{ $user->email }}
                    </div>
                    <div class="col-3">
                        @php
                            $joinDate = new DateTime("$user->date_of_joining");
                            $leavDate = null;
                            if ($user->date_of_leaving == null) {
                                $leavDate = new DateTime(date('Y-m-d'));
                            } else {
                                $leavDate = new DateTime("$user->date_of_leaving");
                            }
                            $diff = $leavDate->diff($joinDate);

                            // print_r($diff);
                            if ($diff->m == 0) {
                                echo $diff->y . ' Years';
                            } else {
                                echo $diff->y . ' Years ' . $diff->m . ' Months';
                            }

                        @endphp
                    </div>
                    <div class="col-2">
                        <!-- Button trigger modal for confirm alert -->
                        <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal"
                            data-bs-target="#confir{{ $user->id }}mAlert">
                            remove
                        </button>
                    </div>
                </div>

                <!-- Modal for confirm alert -->
                <div class="modal fade" id="confir{{ $user->id }}mAlert" tabindex="-1" aria-labelledby="addUser" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-body">
                                        <div class="form-group p-3 text-center">
                                            <h4>Are you sure want to delete ?</h4>
                                        </div>
                                <form action="/remove" method="POST">
                                    @csrf
                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                    <div class="form-group text-center">
                                        <button class="btn btn-danger">Yes</button>
                                        <button type="button" data-bs-dismiss="modal" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="row text-center font-weight-bold p-3 border border-dark">
                <div class="col">
                    NO RECORD FOUND !
                </div>
            </div>

        @endif
    </div>



<script>
    $(document).ready(function(){
        $("#flashMessage").fadeOut(5000);
    });
</script>



</body>

</html>
